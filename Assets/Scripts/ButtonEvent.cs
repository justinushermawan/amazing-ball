﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonEvent : MonoBehaviour {

    public GameObject MainMenu;
    public GameObject ChooseStageMenu;
    public GameObject OptionMenu;

    private bool inAnimationMainMenu = false;
    private bool outAnimationMainMenu = false;

    private bool inAnimationChooseStageMenu = false;
    private bool outAnimationChooseStageMenu = false;

    private bool inAnimationOptionMenu = false;
    private bool outAnimationOptionMenu = false;

	void Start () {
        //MainMenu.SetActive(true);
        //ChooseStageMenu.SetActive(true);
	}
	
	void Update () {
        if (inAnimationMainMenu)
        {
            Vector3 position = MainMenu.GetComponent<RectTransform>().localPosition;
            position.x += 10f;
            MainMenu.GetComponent<RectTransform>().localPosition = position;
            if (position.x >= 0f) inAnimationMainMenu = false;
        }

        if (outAnimationMainMenu)
        {
            Vector3 position = MainMenu.GetComponent<RectTransform>().localPosition;
            position.x -= 10f;
            MainMenu.GetComponent<RectTransform>().localPosition = position;
            if (position.x <= -1000f) outAnimationMainMenu = false;
        }

        if (inAnimationChooseStageMenu)
        {
            Vector3 position = ChooseStageMenu.GetComponent<RectTransform>().localPosition;
            position.x -= 10f;
            ChooseStageMenu.GetComponent<RectTransform>().localPosition = position;
            if (position.x <= -0f) inAnimationChooseStageMenu = false;
        }

        if (outAnimationChooseStageMenu)
        {
            Vector3 position = ChooseStageMenu.GetComponent<RectTransform>().localPosition;
            position.x += 10f;
            ChooseStageMenu.GetComponent<RectTransform>().localPosition = position;
            if (position.x >= 1000f) outAnimationChooseStageMenu = false;
        }

        if (inAnimationOptionMenu)
        {
            Vector3 position = OptionMenu.GetComponent<RectTransform>().localPosition;
            position.x -= 10f;
            OptionMenu.GetComponent<RectTransform>().localPosition = position;
            if (position.x <= -0f) inAnimationOptionMenu = false;
        }

        if (outAnimationOptionMenu)
        {
            Vector3 position = OptionMenu.GetComponent<RectTransform>().localPosition;
            position.x += 10f;
            OptionMenu.GetComponent<RectTransform>().localPosition = position;
            if (position.x >= 1000f) outAnimationOptionMenu = false;
        }
	}

    public void buttonPlayClick()
    {
        outAnimationMainMenu = true;
        inAnimationChooseStageMenu = true;
    }

    public void buttonOptionClick()
    {
        outAnimationMainMenu = true;
        inAnimationOptionMenu = true;
    }

    public void buttonExitClick()
    {
        Application.Quit();
    }

    public void backButtonClick()
    {
        inAnimationMainMenu = true;
        outAnimationChooseStageMenu = true;
        outAnimationOptionMenu = true;
    }

    public void buttonStageClick(Button button)
    {
        //print(button.GetComponentInChildren<Text>().text);
        Application.LoadLevel("Stage1");
    }

}
